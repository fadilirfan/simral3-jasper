#!/bin/bash

IMGNAME="simral-jasperreport-nonroot"
VERSION="1.000"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

pushd $DIR > /dev/null

docker build -t repo.ptik.bppt.go.id:5000/ptik/$IMGNAME:$VERSION .

popd > /dev/null
