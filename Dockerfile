FROM repo.ptik.bppt.go.id:5000/ptik/jasper-wsds:1.001

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt-get update && \
    apt-get install -y default-jdk

ADD entrypoint.sh /entrypoint.sh

ENV JS_DB_HOST=172.17.0.1 \
    JS_DB_PORT=13306 \
    JS_DB_USER=simralcloud \
    JS_DB_PASSWORD=simralcloud

RUN rm -f /root/.jrsks* && \
    /entrypoint.sh install

RUN chmod ugo+rx /entrypoint.sh && \
    find /usr/local/tomcat ! -group 0 -exec chgrp 0 '{}' \; && \
    find /usr/local/tomcat -type d ! -perm 2775 -exec chmod 2775 '{}' \; && \
    find /usr/local/tomcat -type f ! -perm -g+rw -exec chmod g+rw '{}' \; && \
    find /jasperserver ! -group 0 -exec chgrp 0 '{}' \; && \
    find /jasperserver -type d ! -perm 2775 -exec chmod 2775 '{}' \; && \
    find /jasperserver -type f ! -perm -g+rw -exec chmod g+rw '{}' \;    

RUN chmod g=u /etc/passwd
USER 1001:0

ENV JS_DB_HOST=172.17.0.1 \
    JS_DB_PORT=13306 \
    JS_DB_USER=simralcloud \
    JS_DB_PASSWORD=simralcloud

WORKDIR /usr/local/tomcat

EXPOSE 8080
